# TP_esgi

Author : BELMAKDEM Abdelhakim

Compiling
---------

The project can be compiled with the standard command, `mvn compile`

Running
-------
To run the application, type `mvn scala:run`

To run unit-tests, type `mvn test`