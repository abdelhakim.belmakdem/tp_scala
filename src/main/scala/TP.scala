package meteo

import org.apache.spark.sql.functions.{col, udf}

object TP {

  import org.apache.spark.sql.types._
  import org.apache.spark.sql.functions.split
  import org.apache.spark.sql.Encoder

  case class Commune(DEPCOM:String, COM: String, PMUN: String, PCAP: String, PTOT: String)
  case class CodeInsee(
    CODE_INSEE: String, NOM_DE_LA_COMMUNE:String, CODE_POSTAL:String, LIBELLE_D_ACHEMINEMENT:String,
    Y_Centroid:String, Nom_Region:String, Nom_Dept:String, statut:String, Code_Region:String, Code_Commune:String, Z_Moyen:String,
    Code_Dept:String, X_Centroid:String, geom_x_y:String, geom:String, Id_Geofla:String, X_Chef_Lieu:String, Code_Canton:String,
    Superficie:String, Population:String, Nom_Commune:String, Code_Arrondissement:String, Y_Chef_Lieu:String, Code_commune_complet: String
  )

  case class PostsSynop(ID: String, Latitude: String, Longitude: String)
  case class Synop(t: String, numer_sta: String, date: String)

  def main(args: Array[String]) {
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Tp_esgi").getOrCreate

    import spark.sqlContext.implicits._

    val dfCommunes = spark.read
      .schema(implicitly[Encoder[Commune]].schema)
      .option("delimiter", ";")
      .option("ignoreTrailingWhiteSpace", "true")
      .csv(args(0)).as[Commune]

    val dfCodeInseeInitial = spark.read
      .schema(implicitly[Encoder[CodeInsee]].schema)
      .option("delimiter", ";")
      .option("ignoreTrailingWhiteSpace", "true")
      .csv(args(1)).as[CodeInsee]

    val dfCodeInsee = dfCodeInseeInitial.withColumnRenamed("CODE INSEE", "CODE_INSEE")

    val dfJoinPostalCode = dfCommunes.join(
      dfCodeInsee,
      dfCommunes("DEPCOM") === dfCodeInsee("CODE_INSEE"),
      "left"
    )

    // Calculate distances

    val dist = (p1_lat: Double, p1_lon: Double, p2_lat: Double, p2_lon: Double) => {scala.math.sqrt(scala.math.pow(p2_lat-p1_lat,2) + scala.math.pow(p2_lon - p1_lon,2))}

    def dist2(p1_lat: Double, p1_lon: Double, p2_lat:Double, p2_lon:Double) = {scala.math.sqrt(scala.math.pow(p2_lat-p1_lat,2) + scala.math.pow(p2_lon - p1_lon,2))}

    val dist2_udf = udf(dist2 _)
    def dist_udf = udf(dist)

    dfJoinPostalCode.withColumn("_tmp", split($"geom_x_y", ",")).select(
      $"_tmp".getItem(0).as("p1_lat"),
      $"_tmp".getItem(1).as("p1_lon"),
    )

    val dfPostesSynop =  spark.read
      .option("header", "true")
      .option("delimiter", ";")
      .csv(args(2)).as[PostsSynop]
    val dfDailySynop = spark.read
      .option("header", "true")
      .option("delimiter", ";")
      .csv(args(3)).as[Synop]


//    val joinForDistance = dfJoinPostalCode.crossJoin(dfPostesSynop)
//
//    joinForDistance.withColumn("distance", dist_udf(col("p1_lat"),col("p1_lon"),col("Latitude"),col("Longitude"))).show

      dfJoinPostalCode.write.parquet("result.parquet")

  }

}
