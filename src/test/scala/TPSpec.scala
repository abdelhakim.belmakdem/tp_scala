import meteo.TP.Commune
import org.apache.spark.sql.Encoder

import collection.mutable.Stack
import org.scalatest._

class TPSpec extends FlatSpec with Matchers {

  "Import csv" should "create dataframe without error" in {
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Tp_esgi").getOrCreate

    import spark.sqlContext.implicits._

    val dfCommunes = spark.read
      .schema(implicitly[Encoder[Commune]].schema)
      .option("delimiter", ";")
      .option("ignoreTrailingWhiteSpace", "true")
      .csv("test_ressources/Communes.csv").as[Commune]
  }
}
